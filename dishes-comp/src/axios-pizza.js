import axios from 'axios';

const instance = axios.create ({
    baseURL: 'https://pizza-project-fb8cf.firebaseio.com/'
});

export default instance;