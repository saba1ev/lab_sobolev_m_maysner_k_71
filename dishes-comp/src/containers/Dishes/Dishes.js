import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import './Dishes.css';

import {fetchDishes, deleteDish} from "../../store/actions/dishesBuilder";
import Spinner from "../../components/UI/Spinner/Spinner";
import {Container} from "reactstrap";
import {NavLink as RouterLink} from "react-router-dom";
import {NavLink} from 'reactstrap'


class Dishes extends Component {

    componentDidMount() {
        this.props.getDishes();
    }

    render() {
        let dishes = Object.keys(this.props.dishes).map ((key) => {
            return (
                <div className='Dishes' key={key}>
                    <div className="Dish">
                        <div className="dishBtn">
                            <NavLink tag={RouterLink} to='/edit/:id'>
                                <button>Edit</button>
                            </NavLink>
                            <button onClick={() => this.props.deleteDish(key)}>Delete</button>
                        </div>
                        <img src={this.props.dishes[key].image} alt={this.props.dishes[key].image}/>
                        <p className="Name">{this.props.dishes[key].name}</p>
                        <p className="Price">{this.props.dishes[key].price} <strong>KGS</strong></p>

                    </div>
                </div>
            )
        });
        return (
            <Fragment>
                <Container>
                    <div className='Dish'>
                        <h3>Dishes</h3>
                        <NavLink tag={RouterLink} to='/addDish'>
                            <button className='DishBtn'>Add new Dish</button>
                        </NavLink>
                    </div>
                    {this.props.loading ? <Spinner/> : dishes}
                </Container>


            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    dishes: state.dishes.dishes,
    loading: state.dishes.loading
});

const mapDispatchToProps = dispatch => ({
    getDishes: () => dispatch(fetchDishes()),
    deleteDish: (id) => dispatch(deleteDish(id))

});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);