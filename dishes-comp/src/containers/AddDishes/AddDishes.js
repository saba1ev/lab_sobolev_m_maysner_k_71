import React, {Component} from 'react';
import {Container} from "reactstrap";
import './AddDish.css'
import {connect} from "react-redux";
import axios from '../../axios-pizza';

class AddDish extends Component {
    state = {
        name: '',
        price: '',
        image: '',

    };


    valueChanged = (event) =>{
        const {name, value} = event.target;
        this.setState({[name]: value})

    };
    pushDish = () =>{
        const info = {...this.state};
        axios.post(`/dishes.json`, info).then(() =>{
            this.props.history.push('/')
        })

    };
    render() {
        return (
            <Container>
                <div className='addForm'>
                    <h1>Add new Dish</h1>
                    <input type='text' name="name" value={this.state.name} placeholder='name' onChange={this.valueChanged}/>
                    <input type='number' name='price' value={this.state.price} placeholder='price' onChange={this.valueChanged}/>
                    <input type='url' name='image' value={this.state.image} placeholder='image' onChange={this.valueChanged}/>
                    <button onClick={()=>this.pushDish()}>Save</button>
                </div>

            </Container>
        );
    }
}

const mapStateToProps = ()=>{
    return{}
};

const mapDispatchToProps = () =>{
    return{

    }
};

export default connect(mapStateToProps, mapDispatchToProps) (AddDish);