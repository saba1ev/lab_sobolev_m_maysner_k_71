import {DELETE_ORDERS, FETCH_ORDER_ERROR, FETCH_ORDER_REQUEST, FETCH_ORDER_SUCCESS} from "../actions/actionTypes";

const initialState = {
    orders: {},
    loading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ORDER_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_ORDER_SUCCESS:
            return {
                ...state,
                orders: action.orders,
                loading: false,
            };
        case FETCH_ORDER_ERROR:
            return {
                ...state,
                error: action.error
            };
        case DELETE_ORDERS:
            return {
                ...state,
            };
        default:
            return state;
    }
};

export default reducer;
