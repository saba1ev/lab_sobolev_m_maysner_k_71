import {
    FETCH_DISHES_ERROR,
    FETCH_DISHES_ORDERS_SUCCESS,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS
} from "../actions/actionTypes";

const initialState = {
    dishes: {},
    dish: null,
    loading: false,
    error: null,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {
                ...state,
                loading: true
            };
        case FETCH_DISHES_SUCCESS:
            return {
                ...state,
                dishes: action.dishes,
                loading: false,
            };
        case FETCH_DISHES_ORDERS_SUCCESS:
            return {
                ...state,
                dish: action.dish,
                loading: false,
            };
        case FETCH_DISHES_ERROR:
            return {
                ...state,
                error: action.error
            };
        default:
            return state;
    }
};

export default reducer;
