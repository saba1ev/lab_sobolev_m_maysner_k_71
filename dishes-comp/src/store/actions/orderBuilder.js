import axios from '../../axios-pizza';

import {

    FETCH_ORDER_ERROR,
    FETCH_ORDER_REQUEST,
    FETCH_ORDER_SUCCESS,
} from "./actionTypes";

export const fetchOrderRequest = () => {
    return {type: FETCH_ORDER_REQUEST};
};

export const fetchOrderSuccess = orders => {
    return {type: FETCH_ORDER_SUCCESS, orders};
};

export const fetchOrderError = error => {
    return {type: FETCH_ORDER_ERROR, error};
};

export const deleteOrder = (orderId) => {
    return (dispatch) => {
        dispatch(fetchOrderSuccess());
        axios.delete(`/orders/${orderId}.json`).then(() => {
            dispatch(fetchOrders());
        })
    }
};


export const fetchOrders = () => {
    return (dispatch) => {
        dispatch(fetchOrderRequest());
        axios.get('/orders.json').then(response => {
            console.log('orders');
            dispatch(fetchOrderSuccess(response.data));
        }, error => {
            dispatch(fetchOrderError(error));
        });
    }
};