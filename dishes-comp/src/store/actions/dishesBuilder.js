import axios from '../../axios-pizza';

import {
    FETCH_DISHES_ERROR,
    FETCH_DISHES_ORDERS_SUCCESS,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS
} from "./actionTypes";

export const fetchDishesRequest = () => {
    return {type: FETCH_DISHES_REQUEST};
};

export const fetchDishesSuccess = dishes  => {
    return {type: FETCH_DISHES_SUCCESS, dishes};
};

export const fetchDishesOrdesSuccess = dish  => {
    return {type: FETCH_DISHES_ORDERS_SUCCESS, dish};
};

export const fetchDishesError = error => {
    return {type: FETCH_DISHES_ERROR, error};
};

export const fetchDishes = () => {
    return (dispatch) => {
        dispatch(fetchDishesRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(fetchDishesSuccess(response.data));

        }, error => {
            dispatch(fetchDishesError(error));
        });
    }
};
export const fetchDishesOrders = () => {
    return (dispatch) => {
        dispatch(fetchDishesRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(fetchDishesOrdesSuccess(response.data));

        }, error => {
            dispatch(fetchDishesError(error));
        });
    }
};
export const deleteDish = (id) =>{
    return (dispatch) => {
        dispatch(fetchDishesRequest());
        axios.delete(`/dishes/${id}.json`).then(() => {
            dispatch(fetchDishes());
        })
    }
};