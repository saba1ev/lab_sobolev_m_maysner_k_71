import React, {Component} from 'react';
import {connect} from "react-redux";
import './Orders.css';


import {deleteOrder, fetchOrders} from "../../store/actions/orderBuilder";
import {fetchDishesOrders} from "../../store/actions/dishesBuilder";


class Orders extends Component {


    componentDidMount() {
        this.props.getOrders();
        this.props.getDishes();
    }


    render() {
        if (!this.props.dish || !this.props.orders) {
            return <div>...Loading</div>
        }
        const orders = Object.keys(this.props.orders).map(orderId => {
            let orderPrice = 0;
            let order = this.props.orders[orderId];

            return (
                <div className="orders" key={orderId}>
                    <p><strong>Order Id: {orderId}</strong></p>
                    {Object.keys(order).map(dishesId => {
                        const dish = this.props.dish[dishesId];
                        let orderQty = order[dishesId];
                        let orderItemPrice = dish.price * orderQty;

                        orderPrice += orderItemPrice;
                        return (
                            <p key={dishesId}><strong>{dish.name}</strong>: x{orderQty} = {orderItemPrice}</p>
                        )
                    })
                    }
                    <p><strong>Delivery Prcie: </strong> 150</p>
                    <p><strong>Total price: </strong>{orderPrice + 150}</p>
                    <button onClick={() => this.props.deleteOrder(orderId)}>Complete order</button>
                </div>
            )
        });
        return (
            <div>
                {orders}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    orders: state.orders.orders,
    dish: state.dishes.dish,
    loading: state.orders.loading
});

const mapDispatchToProps = dispatch => ({
    getOrders: () => dispatch(fetchOrders()),
    getDishes: () => dispatch(fetchDishesOrders()),
    deleteOrder: (orderId) => dispatch(deleteOrder(orderId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);