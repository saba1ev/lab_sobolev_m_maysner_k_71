import React, {Component} from 'react';
import axios from "../../axios-pizza";


class DishForm extends Component {


    constructor(props) {
        super(props);
        if (this.props.dishes) {
            this.state = {...props.dishes}
        } else {
            this.state = {
                name: '',
                price: '',
                image: '',
            };
        }
    }

    valueChanged = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };


    render() {
        return (
            <div className='addForm' onChange={this.submitHandler}>
                <h1>Add new Dish</h1>
                <input type='url' name='name' value={this.state.name} placeholder='name' onChange={this.valueChanged}/>
                <input type='number' name='price' value={this.state.price} placeholder='price'
                       onChange={this.valueChanged}/>
                <input type='text' name='image' value={this.state.image} placeholder='image'
                       onChange={this.valueChanged}/>
                <button type='submit'>Save</button>
            </div>
        );
    }
}

export default DishForm;
