import React from 'react';

import './Logo.css';
import logo from '../../assets/images/pizza-logo.png';


const Logo = () => (
    <div className="Logo">
        <img src={logo} alt="MyBurger" />
    </div>
);

export default Logo;
