import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import Layout from "./components/Layout/Layout";
import Dishes from "./containers/Dishes/Dishes";
import Orders from "./components/Orders/Orders";
import AddDishes from "./containers/AddDishes/AddDishes";







class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>

                    <Route path='/addDish' component={AddDishes}/>
                    <Route path="/orders" component={Orders}/>
                    <Route path="/" component={Dishes}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;